package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 
    	 
    	 return lista;
     }

     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
     {
    	 
    	 int n1 = izquierda.length;
    	 int n2 = derecha.length;
    	 Comparable[] listaMerged = new Comparable[n1+n2];
    	 
    	 Comparable[] L = new Comparable[n1];
    	 Comparable[] R = new Comparable[n2];
    	 
    	 for (int i = 0; i < n1; i++)
    	 {
    		 L[i] = listaMerged[n1+i];
    	 }
    	 for (int j = 0; j < n2; j++)
    	 {
    		 R[j] = listaMerged[n2+1+j];
    	 }
    	 
    	 int i = 0, j = 0;
    	 
    	 int k = n2;
    	 while (i < n1 && j < n2)
    	 {
    		 if(L[i].compareTo(R[j]) <= 0)
    		 {
    			 listaMerged[k] = L[i];
    			 i++;
    		 }
    		 else
    		 {
    			 listaMerged[k] = R[j];
    			 j++;
    		 }
    		 k++;
    	 }
    	 
    	 while (i < n1)
    	 {
    		 listaMerged[k] = L[i];
    		 i++;
    		 k++;
    	 }
    	 
    	 while (j < n2)
    	 {
    		 listaMerged[k] = R[j];
    		 j++;
    		 k++;
    	 }
    	 
    	 
    	 return listaMerged;
     }


}
